<?php

/**
 * Class Cassian_Positioner_Block_Adminhtml_Positioner_Edit_Tab_Form
 * @author Hector Fernandez <hector.nek@gmail.com>
 * @access Public
 * @package Cassian_Positioner
 * @copyright Copyright (c) 2018 X.commerce, Inc. and affiliates (http://www.magento.com)
 */
class Cassian_Positioner_Block_Adminhtml_Positioner_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
		protected function _prepareForm()
		{
            /**
             * Instance form
             */
				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("positioner_form", array("legend"=>Mage::helper("positioner")->__("Item information")));

								
						$fieldset->addField('filename', 'file', array(
						'label' => Mage::helper('positioner')->__('File'),
						'name' => 'filename',
						'note' => '(*.csv)',
						));

				if (Mage::getSingleton("adminhtml/session")->getPositionerData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getPositionerData());
					Mage::getSingleton("adminhtml/session")->setPositionerData(null);
				} 
				elseif(Mage::registry("positioner_data")) {
				    $form->setValues(Mage::registry("positioner_data")->getData());
				}
				return parent::_prepareForm();
		}
}
