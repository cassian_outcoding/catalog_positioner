<?php

/**
 * Class Cassian_Positioner_Block_Adminhtml_Positioner_Edit_Tabs
 * @author Hector Fernandez <hector.nek@gmail.com>
 * @access Public
 * @package Cassian_Positioner
 * @copyright Copyright (c) 2018 X.commerce, Inc. and affiliates (http://www.magento.com)
 */
class Cassian_Positioner_Block_Adminhtml_Positioner_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Cassian_Positioner_Block_Adminhtml_Positioner_Edit_Tabs constructor.
     */
		public function __construct()
		{
				parent::__construct();
				$this->setId("positioner_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("positioner")->__("File Information"));
		}

    /**
     * @return Mage_Core_Block_Abstract
     * @throws Exception
     */
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("positioner")->__("File Information"),
				"title" => Mage::helper("positioner")->__("File Information"),
				"content" => $this->getLayout()->createBlock("positioner/adminhtml_positioner_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
