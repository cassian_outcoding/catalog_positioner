<?php

/**
 * Class Cassian_Positioner_Block_Adminhtml_Positioner_Grid
 * @author Hector Fernandez <hector.nek@gmail.com>
 * @access Public
 * @package Cassian_Positioner
 * @copyright Copyright (c) 2018 X.commerce, Inc. and affiliates (http://www.magento.com)
 */
class Cassian_Positioner_Block_Adminhtml_Positioner_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Cassian_Positioner_Block_Adminhtml_Positioner_Grid constructor.
     */
		public function __construct()
		{
				parent::__construct();
				$this->setId("positionerGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
		protected function _prepareCollection()
		{
				$collection = Mage::getModel("positioner/positioner")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     * @throws Exception
     */
		protected function _prepareColumns()
		{
				$this->addColumn("id", array(
				"header" => Mage::helper("positioner")->__("ID"),
				"align" =>"right",
				"width" => "50px",
                                "type" => "number",
				"index" => "id",
				));
                                
                                $this->addColumn("filename", array(
				"header" => Mage::helper("positioner")->__("File Name"),
				"index" => "filename",
				));
                                
                                
				$this->addColumn("user", array(
				"header" => Mage::helper("positioner")->__("User"),
				"index" => "user",
				));
                                
                                $this->addColumn("process_state", array(
				"header" => Mage::helper("positioner")->__("Process State"),
				"index" => "process_state",
				));
                                
                                
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

    /**
     * @param $row
     * @return string
     */
		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


    /**
     * @return $this|Mage_Adminhtml_Block_Widget_Grid
     */
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_positioner', array(
					 'label'=> Mage::helper('positioner')->__('Remove Positioner'),
					 'url'  => $this->getUrl('*/adminhtml_positioner/massRemove'),
					 'confirm' => Mage::helper('positioner')->__('Are you sure?')
				));
			return $this;
		}
			

}