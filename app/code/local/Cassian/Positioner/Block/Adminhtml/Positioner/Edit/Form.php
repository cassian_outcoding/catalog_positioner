<?php

/**
 * Class Cassian_Positioner_Block_Adminhtml_Positioner_Edit_Form
 * @author Hector Fernandez <hector.nek@gmail.com>
 * @access Public
 * @package Cassian_Positioner
 * @copyright Copyright (c) 2018 X.commerce, Inc. and affiliates (http://www.magento.com)
 */
class Cassian_Positioner_Block_Adminhtml_Positioner_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     * @throws Exception
     */
		protected function _prepareForm()
		{
				$form = new Varien_Data_Form(array(
				"id" => "edit_form",
				"action" => $this->getUrl("*/*/save", array("id" => $this->getRequest()->getParam("id"))),
				"method" => "post",
				"enctype" =>"multipart/form-data",
				)
				);
				$form->setUseContainer(true);
				$this->setForm($form);
				return parent::_prepareForm();
		}
}
