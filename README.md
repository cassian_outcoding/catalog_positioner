# Cassian Magento 1.X Catalog Positioner

This module enable the user to update catalog product order position inside the catalog categories.
Also add or update previous products on category. 

* Author: Héctor Fernández 
* User: @cassian42 
* <hector.nek@outlook.com> 

## Getting Started

These instructions will let you know how to install and deploy the module on your Magento Application
 
### Prerequisites

* PHP 5.3 >
* Mysql5/ MariaDB5 
* Magento 1.7.x > 1.9.x
* APACHE / NGINX / IIS 


### Installing

* Clone or download repository

```
$ git clone https://bitbucket.org/cassian42/catalog_positioner.git 
```

if you download the repository as ZIP

```
$ unzip catalog_positioner.zip
```

Copy the 'app' directory inside the root of your magento directory

```
$ cp catalog_positioner/app/* your_magento_root_directory/app/*
```
Then

* Sign in your Magento Admin
* Go to System > Configuration > catalog > Positioner
* Set status as enabled  (if you get 404 error here please try  to logout out and login again  )
* Refresh Caché

## Running the tests

* Go to Catalog -> Positioner -> Manage Files
* click on add new file button.
* Upload a valid CSV file

Valid CSV Format 

```
category_id,sku,position
2,TST0001,1
2,TST0002,2
2,TST0003,3
2,TST0004,4
2,TST0005,5
3,TST0001,5
3,TST0002,4
3,TST0003,3
3,TST0004,2
3,TST0005,1
4,TST0001,5
4,TST0002,3
4,TST0003,1
4,TST0004,2
4,TST0005,4
5,TST0001,1
5,TST0002,2
5,TST0003,1
5,TST0004,1
5,TST0005,1
```

* [See the video](https://drive.google.com/file/d/1lbehsBLByjgebIPEilDMLUBfLw4y8okm/view)


## Authors

* **Héctor Fernández** - *Magento Developer* - [LinkedIN](https://www.linkedin.com/in/hector-nek/)


## License

This project is licensed under the MIT License - see the [LICENSE.md](https://en.wikipedia.org/wiki/MIT_License) file for details
